<?php

namespace Drupal\social_auth_ok\Plugin\Network;

use Drupal\social_auth\Plugin\Network\NetworkInterface;

/**
 * Defines the Odnoklassniki Auth interface.
 */
interface OdnoklassnikiAuthInterface extends NetworkInterface {}
