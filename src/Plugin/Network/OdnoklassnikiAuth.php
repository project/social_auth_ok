<?php

namespace Drupal\social_auth_ok\Plugin\Network;

use Drupal\Core\Url;
use Drupal\social_api\SocialApiException;
use Drupal\social_auth\Plugin\Network\NetworkBase;
use Drupal\social_auth_ok\Settings\OdnoklassnikiAuthSettings;
use Max107\OAuth2\Client\Provider\Odnoklassniki;

/**
 * Defines a Network Plugin for Social Auth Odnoklassniki.
 *
 * @Network(
 *   id = "social_auth_ok",
 *   social_network = "Odnoklassniki",
 *   type = "social_auth",
 *   handlers = {
 *     "settings": {
 *       "class": "\Drupal\social_auth_ok\Settings\OdnoklassnikiAuthSettings",
 *       "config_id": "social_auth_ok.settings"
 *     }
 *   }
 * )
 */
class OdnoklassnikiAuth extends NetworkBase implements OdnoklassnikiAuthInterface {

  /**
   * Sets the underlying SDK library.
   *
   * @return false|Odnoklassniki
   *   The initialized 3rd party library instance.
   *
   * @throws SocialApiException
   *   If the SDK library does not exist.
   */
  protected function initSdk() {

    $class_name = '\Max107\OAuth2\Client\Provider\Odnoklassniki';
    if (!class_exists($class_name)) {
      throw new SocialApiException(sprintf('The OAuth2 library for Odnoklassniki not found. Class: %s.', $class_name));
    }

    /* @var \Drupal\social_auth_ok\Settings\OdnoklassnikiAuthSettings $settings */
    $settings = $this->settings;

    if ($this->validateConfig($settings)) {
      // All these settings are mandatory.
      $league_settings = [
        'clientId' => $settings->getClientId(),
        'clientPublic' => $settings->getClientPublic(),
        'clientSecret' => $settings->getClientSecret(),
        'redirectUri' => Url::fromRoute('social_auth_ok.callback')->setAbsolute()->toString(),
      ];

      // Proxy configuration data for outward proxy.
      $proxyUrl = $this->siteSettings->get('http_client_config')['proxy']['http'];
      if ($proxyUrl) {
        $league_settings = [
          'proxy' => $proxyUrl,
        ];
      }

      return new Odnoklassniki($league_settings);
    }

    return FALSE;
  }

  /**
   * Checks that module is configured.
   *
   * @param \Drupal\social_auth_ok\Settings\OdnoklassnikiAuthSettings $settings
   *   The Odnoklassniki auth settings.
   *
   * @return bool
   *   True if module is configured.
   *   False otherwise.
   */
  protected function validateConfig(OdnoklassnikiAuthSettings $settings) {
    $client_id = $settings->getClientId();
    $client_public = $settings->getClientPublic();
    $client_secret = $settings->getClientSecret();

    if (!$client_id || !$client_public || !$client_secret) {
      $this->loggerFactory
        ->get('social_auth_ok')
        ->error('Define Client ID, Client Public and Client Secret on module settings.');
      return FALSE;
    }

    return TRUE;
  }

}
