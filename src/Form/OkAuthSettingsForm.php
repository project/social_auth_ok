<?php

namespace Drupal\social_auth_ok\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\social_auth\Form\SocialAuthSettingsForm;

/**
 * Class OkAuthSettingsForm.
 */
class OkAuthSettingsForm extends SocialAuthSettingsForm {

  /**
   * Drupal\Core\Routing\RequestContext definition.
   *
   * @var \Drupal\Core\Routing\RequestContext
   */
  protected $requestContext;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_auth_ok_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return array_merge(
      parent::getEditableConfigNames(),
      ['social_auth_ok.settings']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('social_auth_ok.settings');

    $form['ok_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Odnoklassniki Client settings'),
      '#open' => TRUE,
      '#description' => $this->t('You need to first create a Odnoklassniki App at <a href="@ok-dev">@ok-dev</a>',
        ['@ok-dev' => 'https://apiok.ru/en/dev/app/create']),
    ];

    $form['ok_settings']['client_id'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('client_id'),
      '#description' => $this->t('Copy the Client ID here.'),
    ];

    $form['ok_settings']['client_public'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Client Public Key'),
      '#default_value' => $config->get('client_public'),
      '#description' => $this->t('Copy the Client Public here.'),
    ];

    $form['ok_settings']['client_secret'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Client Secret Key'),
      '#default_value' => $config->get('client_secret'),
      '#description' => $this->t('Copy the Client Secret here.'),
    ];

    $form['ok_settings']['authorized_redirect_url'] = [
      '#type' => 'textfield',
      '#disabled' => TRUE,
      '#title' => $this->t('Authorized redirect URIs'),
      '#description' => $this->t('Copy this value to <em>Permitted redirect_uri</em> field of your Odnoklassniki App settings.'),
      '#default_value' => Url::fromRoute('social_auth_ok.callback')->setAbsolute()->toString(),
    ];

    $form['google_settings']['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced settings'),
      '#open' => FALSE,
    ];

    $form['ok_settings']['advanced']['scopes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Scopes for API call'),
      '#default_value' => $config->get('scopes'),
      '#description' => $this->t('Define any additional scopes to be requested, separated by a comma (e.g.: VALUABLE_ACCESS,GROUP_CONTENT).<br>
                                  The scope  \'GET_EMAIL\' is added by default and always requested.<br>
                                  You can see the full list of valid scopes and their description <a href="@scopes">here</a>.', ['@scopes' => 'https://apiok.ru/en/ext/oauth/permissions']),
    ];

    $form['ok_settings']['advanced']['endpoints'] = [
      '#type' => 'textarea',
      '#title' => $this->t('API calls to be made to collect data'),
      '#default_value' => $config->get('endpoints'),
      '#description' => $this->t('Define the endpoints to be requested when user authenticates with Odnoklassniki for the first time<br>
                                  A list of endpoints can be found <a href="@endpoints">here</a> <br>
                                  Enter each endpoint in a different line in the format <em>endpoint</em>|<em>name_of_endpoint</em>.<br>
                                  <b>For instance:</b><br>
                                  /api/friends/get|user_friends<br>', ['@enpoints' => 'https://apiok.ru/en/dev/methods/rest/']),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('social_auth_ok.settings')
      ->set('client_id', $values['client_id'])
      ->set('client_public', $values['client_public'])
      ->set('client_secret', $values['client_secret'])
      ->set('scopes', $values['scopes'])
      ->set('endpoints', $values['endpoints'])
      ->save();
  }

}
